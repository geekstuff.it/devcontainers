ARG IMAGE
ARG TAG
ARG KIT
FROM ${IMAGE}:${TAG}

# Copy devcontainer scripts
COPY ./scripts/ /devcontainer/

# Setup devcontainer
RUN /devcontainer/basics.sh

# Run user/ scripts (TODO missing vars to really test some)
RUN /devcontainer/user/kubeconfig.sh
RUN /devcontainer/user/netrc.sh
RUN /devcontainer/user/starship.sh

# Run every tools/ scripts
RUN /devcontainer/tools/direnv.sh \
    && direnv version

RUN /devcontainer/tools/docker.sh \
    && docker --version

RUN /devcontainer/tools/docker-compose.sh \
    && docker-compose version

RUN /devcontainer/tools/kubectl.sh \
    && kubectl --client=true version

RUN /devcontainer/tools/helm.sh \
    && helm version

RUN /devcontainer/tools/rancher-cli.sh \
    && rancher --version

RUN /devcontainer/tools/terraform.sh \
    && terraform version

RUN /devcontainer/tools/vault.sh \
    && vault version

# Run kit if specified
RUN test -z "${KIT}" || /devcontainer/kits/${KIT}.sh
