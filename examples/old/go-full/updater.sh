#!/bin/sh

# Eventual script / idea

# Scans devcontainer.json and docker-compose-base.yml and self
# if anything missing:
#   - check if the files are currently git tracked otherwise error out
#   - same if they have uncomitted changes to them
#   - if updater.sh needs updating, apply and error out saying to retry?
#   - append/merge devcontainer.json
#   - replace for docker-compose-base.yml
#
#
# This is done manually by the user or could also be done from a CI with
# extra steps to create a branch and a merge/pull request.
