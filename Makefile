#!make

IMAGE ?= devcontainer
TAG ?= dev

all: docker

.PHONY: docker
docker:
	docker build -t ${IMAGE}:${TAG} .
	@echo "Built docker image ${IMAGE}:${TAG}"

.PHONY: docker-local-debian
docker-local-debian:
	@echo "[] Run a local docker mount to test scripts"
	docker run --rm -it -v $(shell pwd)/scripts:/devcontainer debian:stretch-slim sh

.PHONY: docker-local-go-alpine
docker-local-go-alpine:
	@echo "[] Run a local docker mount to test scripts"
	docker run --rm -it -v $(shell pwd)/scripts:/devcontainer golang:1.17-alpine sh

.PHONY: shellcheck
shellcheck:
	@echo "[] Shellcheck"
	@if ! command -v shellcheck >/dev/null 2>&1; then \
		echo "Please install 'shellcheck'"; \
		exit 1; \
	fi
	@find scripts -type f -exec shellcheck -s dash {} \;
