#!/bin/sh

# Create a non-root "dev" user
# Add sudo support

if test -z "$COMMON_LOADED"; then
    . "$(dirname "$0")"/../.common.sh
fi

# vars with default values that can be overriden and comitted to project
requireEnvs "DEV_USERNAME"

# vars with default values that can be overridden and comitted to project,
# and should also be overridable by users with their own uncomitted values.
requireEnvs "DEV_UID DEV_GID"

if isApk; then
    addgroup -g $DEV_GID $DEV_USERNAME \
        && adduser --disabled-password -s /bin/bash --uid $DEV_UID -G $DEV_USERNAME $DEV_USERNAME \
        && echo $DEV_USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$DEV_USERNAME \
        && chmod 0440 /etc/sudoers.d/$DEV_USERNAME \
        && mkdir -p /home/${DEV_USERNAME}/.config \
        && chown ${DEV_USERNAME}: /home/${DEV_USERNAME}/.config

elif isApt; then
    groupadd --gid $DEV_GID $DEV_USERNAME \
        && useradd -s /bin/bash --uid $DEV_UID --gid $DEV_GID -m $DEV_USERNAME \
        && apt-get update \
        && apt-get install -y sudo \
        && rm -rf /var/lib/apt/lists/* \
        && echo $DEV_USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$DEV_USERNAME \
        && chmod 0440 /etc/sudoers.d/$DEV_USERNAME \
        && mkdir -p /home/${DEV_USERNAME}/.config \
        && chown ${DEV_USERNAME}: /home/${DEV_USERNAME}/.config
fi

# At this point we can assume bash is installed for both
# alpine and debian images, so we only need to fix the alpine
# case most likely
if ! test -e /home/${DEV_USERNAME}/.profile; then
    cat << 'EOF' > /home/${DEV_USERNAME}/.profile
# load bash completion
source /etc/profile.d/bash_completion.sh

# include .bashrc if it exists
if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
EOF
    chown ${DEV_USERNAME}: /home/${DEV_USERNAME}/.profile
fi

out "[] Dev container user $DEV_USERNAME is configured"
