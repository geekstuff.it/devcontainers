#!/bin/sh

# Permit VS Code to find the installed extensions after a container rebuild

if test -z "$COMMON_LOADED"; then
    . "$(dirname "$0")"/../.common.sh
fi

# vars with default values that can be overriden and comitted to project
requireEnvs "DEV_USERNAME"

mkdir -p /home/$DEV_USERNAME/.vscode-server/extensions \
        /home/$DEV_USERNAME/.vscode-server-insiders/extensions \
    && chown -R $DEV_USERNAME \
        /home/$DEV_USERNAME/.vscode-server \
        /home/$DEV_USERNAME/.vscode-server-insiders

out "[] VSCode tweaks are applied"
