#!/bin/sh

# Install Vault

. "$(dirname "$0")"/../.common.sh

curl -sfL https://releases.hashicorp.com/vault/1.9.4/vault_1.9.4_linux_amd64.zip -o vault.zip
unzip vault.zip
mv vault /usr/local/bin
rm vault.zip

out "[] Vault CLI is installed"
