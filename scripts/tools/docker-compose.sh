#!/bin/sh

# Install docker-compose

. "$(dirname "$0")"/../.common.sh

if ! command -v docker > /dev/null 2>&1; then
    errorOut "docker is required for docker-compose"
fi

if test -z "$DOCKER_COMPOSE_VERSION"; then
    # TODO Fetch latest 1.x version
    DOCKER_COMPOSE_VERSION=1.29.1
fi

if isApk; then
    apk update
    apk add --no-cache docker-compose
    rm -rf /var/cache/apk/*
elif isApt; then
    curl -sSL "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" \
        -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
fi

out "[] docker-compose is installed"
