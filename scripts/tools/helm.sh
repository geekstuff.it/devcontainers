#!/bin/sh

# Install Helm

. "$(dirname "$0")"/../.common.sh

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
rm ./get_helm.sh

out "[] Helm is installed"
