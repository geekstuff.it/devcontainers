#!/bin/sh

# Install rancher cli

. "$(dirname "$0")"/../.common.sh

# vars with default values that can be overriden and comitted to project
if test -z "$RANCHER_VERSION"; then
    # TODO fetch the latest version
    RANCHER_VERSION=v2.4.11
fi

curl -sfL -o rancher-cli.tgz \
    https://releases.rancher.com/cli2/${RANCHER_VERSION}/rancher-linux-amd64-${RANCHER_VERSION}.tar.gz
tar zxvf rancher-cli.tgz --strip-components=2
mv rancher /usr/local/bin
rm rancher-cli.tgz

out "[] rancher cli is installed"
