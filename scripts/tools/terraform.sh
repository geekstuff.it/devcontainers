#!/bin/sh

# Install Terraform

. "$(dirname "$0")"/../.common.sh

if isApk; then
    apk update
    apk add --no-cache terraform
    rm -rf /var/cache/apk/*
elif isApt; then
    apt-get update
    apt-get install -y gnupg software-properties-common curl
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    apt-get update
    apt-get install terraform
    rm -rf /var/lib/apt/lists/*
fi

out "[] Terraform is installed"
