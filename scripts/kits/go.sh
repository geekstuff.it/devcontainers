#!/bin/sh

# Install VSCode GO dependencies, on top of a GO image.
# This does not install GO itself.

. "$(dirname "$0")"/../.common.sh

if ! test -d /go; then
    errorOut "This script is meant to run on go base images"
fi

# vars with default values that can be overriden and comitted to project
requireEnvs "DEV_USERNAME"

# For alpine, add GCC
if isApk && ! command -v gcc >/dev/null 2>&1; then
    apk update
    apk add --no-cache build-base
    rm -rf /var/cache/apk/*
fi

# Move GOPATH
cd /
mv /go /home/${DEV_USERNAME}
echo "GOPATH=/home/${DEV_USERNAME}/go" >> /home/${DEV_USERNAME}/.profile
echo 'PATH=~/go/bin:/usr/local/go/bin:$PATH' >> /home/${DEV_USERNAME}/.profile
if isApk; then
    echo "export GO111MODULE=on" >> /home/${DEV_USERNAME}/.profile
fi

# Add write permission for /go for dev user
chown -R ${DEV_USERNAME}: /home/${DEV_USERNAME}/go

# golang tools
sudo -u ${DEV_USERNAME} bash -l <<'EOF'
go install -v golang.org/x/tools/gopls@latest

# # golangci-lint https://github.com/golangci/golangci-lint
# curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b ~/go/bin 2>&1

# # TODO go 1.17 mentions go get in a non-module context is deprecated and should be go install
# #      but some of the packages here don't seem to like go install.
# go get -v golang.org/x/tools/gopls@latest 2>&1 \
#     && go get -v \
#         honnef.co/go/tools/...@latest \
#         github.com/go-delve/delve/cmd/dlv@latest \
#         github.com/ramya-rao-a/go-outline@latest \
#         github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest \
#         golang.org/x/tools/cmd/gorename@latest \
#         golang.org/x/tools/cmd/goimports@latest \
#         golang.org/x/lint/golint@latest \
#     && ln -s ~/go/bin/dlv ~/go/bin/dlv-dap
EOF

out "[] VSCode GO dependencies are installed"
