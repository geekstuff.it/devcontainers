#!/bin/sh

# Setup user ~/.netrc

. "$(dirname "$0")"/../.common.sh

# vars with default values that can be overriden and comitted to project
requireEnvs "DEV_USERNAME"

# vars that should be overridable by users with their own uncomitted values.
if ! envsPresent "NETRC_HOST NETRC_USER NETRC_PASSWORD"; then
    skipExecution
fi

echo "machine ${NETRC_HOST} login ${NETRC_USER} password ${NETRC_PASSWORD}" > /home/${DEV_USERNAME}/.netrc
chown ${DEV_USERNAME}: /home/${DEV_USERNAME}/.netrc
chmod go-rwx /home/${DEV_USERNAME}/.netrc

out "[] /home/${DEV_USERNAME}/.netrc is setup"
