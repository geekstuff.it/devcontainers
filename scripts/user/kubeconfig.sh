#!/bin/sh

# Setup user kubeconfig

. "$(dirname "$0")"/../.common.sh

# vars with default values that can be overriden and comitted to project
requireEnvs "DEV_USERNAME"

# vars that should be overridable by users with their own uncomitted values.
if ! envsPresent "KUBECONFIG"; then
    skipExecution
fi

mkdir -p /home/${DEV_USERNAME}/.kube
if isApk; then
    echo -n ${KUBECONFIG} | base64 -d > /home/${DEV_USERNAME}/.kube/config
elif isApt; then
    echo -n ${KUBECONFIG} | base64 --decode > /home/${DEV_USERNAME}/.kube/config
fi
chown -R ${DEV_USERNAME}: /home/${DEV_USERNAME}/.kube
chmod -R go-rwx /home/${DEV_USERNAME}/.kube

out "[] User kubeconfig is setup"
