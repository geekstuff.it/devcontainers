#!/bin/sh

# Add essential devcontainer packages

baseDir="$(dirname "$0")"
. $baseDir/.common.sh

. $baseDir/basics/1-packages.sh
. $baseDir/basics/2-user.sh
. $baseDir/basics/3-vscode.sh
. $baseDir/basics/4-gitconfig.sh
