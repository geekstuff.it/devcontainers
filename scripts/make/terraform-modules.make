#!make

# This make file generates a bunch of make targets
# for each terraform module folders $TF_MODULES

# Below are loops of give all TF targets for all $TF_MODULES
# The namings are geared to help with make auto-completion (tabs and visual ordering)

# tf module all stages jobs
TF_MODULES_ALL := $(shell echo ${TF_MODULES} | tr ' ' '\n' | sed -E 's/(.*)/tfm-\1/')
.PHONY: tfa $(TF_MODULES_ALL)
tfa: $(TF_MODULES_ALL)
$(TF_MODULES_ALL): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2-))
	$(eval TF_MODULE_BARE := $(shell echo $* | cut -d'-' -f2-))
	@echo "[] terraform module $(TF_MODULE)"
	@$(MAKE) tfm-$(TF_MODULE_BARE)-init
	@$(MAKE) tfm-$(TF_MODULE_BARE)-fmt
	@$(MAKE) tfm-$(TF_MODULE_BARE)-plan
	@$(MAKE) tfm-$(TF_MODULE_BARE)-apply

# tf init jobs
TF_MODULES_INIT := $(shell echo ${TF_MODULES} | tr ' ' '\n' | sed -E 's/(.*)/tfm-\1-init/')
.PHONY: tfa-init $(TF_MODULES_INIT)
tfa-init: $(TF_MODULES_INIT)
$(TF_MODULES_INIT): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2- | rev | cut -d'-' -f2- | rev))
	@echo "[] terraform $(TF_MODULE)"
	@DIR=$(TF_MODULE) $(MAKE) .direnv-allow
	@cd "$(TF_MODULE)" && TF_LOG=${TF_LOG} direnv exec . terraform init -input=false

# tf fmt job
TF_MODULES_FMT := $(shell echo ${TF_MODULES} | tr ' ' '\n' | sed -E 's/(.*)/tfm-\1-fmt/')
.PHONY: tfa-fmt $(TF_MODULES_FMT)
tfa-fmt: $(TF_MODULES_FMT)
$(TF_MODULES_FMT): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2- | rev | cut -d'-' -f2- | rev))
	@echo "[] terraform fmt $(TF_MODULE)"
	@DIR=$(TF_MODULE) $(MAKE) .direnv-allow
	@cd "$(TF_MODULE)" && TF_LOG=${TF_LOG} terraform fmt

# tf plan jobs
TF_MODULES_PLAN := $(shell echo ${TF_MODULES} | tr ' ' '\n' | sed -E 's/(.*)/tfm-\1-plan/')
.PHONY: $(TF_MODULES_PLAN)
$(TF_MODULES_PLAN): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2- | rev | cut -d'-' -f2- | rev))
	@echo "[] terraform plan $(TF_MODULE)"
	@DIR=$(TF_MODULE) $(MAKE) .direnv-allow
	@cd "$(TF_MODULE)" && TF_LOG=${TF_LOG} direnv exec . terraform plan -out=.tfplan -input=false

# tf apply jobs
TF_MODULES_APPLY := $(shell echo ${TF_MODULES} | tr ' ' '\n' | sed -E 's/(.*)/tfm-\1-apply/')
.PHONY: $(TF_MODULES_APPLY)
$(TF_MODULES_APPLY): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2- | rev | cut -d'-' -f2- | rev))
	@echo "[] terraform apply $(TF_MODULE)"
	@DIR=$(TF_MODULE) $(MAKE) .direnv-allow
	@cd "$(TF_MODULE)" && TF_LOG=${TF_LOG} direnv exec . terraform apply -input=false .tfplan

# tf destroy jobs (with reverse modules order)
TF_MODULES_DESTROY := $(shell echo ${TF_MODULES} | tr ' ' '\n' | tac | sed -E 's/(.*)/tfm-\1-destroy/')
.PHONY: tfa-destroy $(TF_MODULES_DESTROY)
tfa-destroy: $(TF_MODULES_DESTROY)
$(TF_MODULES_DESTROY): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2- | rev | cut -d'-' -f2- | rev))
	@echo "[] terraform destroy $(TF_MODULE)"
	@DIR=$(TF_MODULE) $(MAKE) .direnv-allow
	@cd "$(TF_MODULE)" && TF_LOG=${TF_LOG} direnv exec . terraform destroy || true

# direnv allow if not already allowed
.PHONY: .direnv-allow
.direnv-allow:
	@if ! grep -Rq "$(DIR)" ~/.local/share/direnv/allow 2>/dev/null; then \
		echo "[] direnv: allow dir $(DIR)"; \
		direnv allow "$(DIR)"; \
	fi

# tf update module sources (TODO Fix)
TF_MODULES_UPDATE := $(shell echo ${TF_MODULES} | tr ' ' '\n' | sed -E 's/(.*)/tfm-\1-update/')
.PHONY: tfa-update $(TF_MODULES_UPDATE)
tfa-update: $(TF_MODULES_UPDATE)
$(TF_MODULES_UPDATE): %:
	$(eval TF_MODULE := $(shell echo $* | cut -d'-' -f2- | rev | cut -d'-' -f2- | rev))
	@echo "[] Update terraform module (tf get -update) $(TF_MODULE)"
	@DIR=$(TF_MODULE) $(MAKE) .direnv-allow
	@cd "$(TF_MODULE)" && terraform get -update
