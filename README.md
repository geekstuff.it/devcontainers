# devcontainer, on almost any image

This repo holds a series of simple scripts to use [VSCode devcontainers](https://code.visualstudio.com/docs/remote/containers)
with any base images.

The base and the current tools scripts are able to handle both `apt` and `apk` scenario.

## Features

Basics:

- non-root user
- vscode dependencies
- ssh agent support
- gpg agent support

Easily installable tools:

- docker (within the devcontainer)
- docker-compose
- helm
- kubectl
- terraform

Development kits:

- go

## How to use

(better example are coming)

Debian:

```Dockerfile
FROM debian:stretch-slim

# User customizable values
ARG GIT_EMAIL
ARG GIT_SIGNINGKEY

# Copy devcontainer scripts
COPY --from=geekstuffreal/devcontainer:v0.7 /scripts/ /devcontainer/

# Setup devcontainer
RUN GIT_EMAIL=${GIT_EMAIL} \
    GIT_SIGNINGKEY=${GIT_SIGNINGKEY} \
        /devcontainer/basics.sh \
    && /devcontainer/tools/kubectl.sh \
    && /devcontainer/tools/helm.sh \
    && /devcontainer/user/starship.sh
```

Alpine:

```Dockerfile
# Copy the same content as above and just replace the FROM line.
FROM alpine:3
```

Golang:

```Dockerfile
FROM golang:1.17-stretch

# User customizable values
ARG GIT_EMAIL
ARG GIT_SIGNINGKEY
ARG KUBECONFIG

# Copy devcontainer scripts
#COPY --from=geekstuffreal/devcontainer:v0.7 /scripts/ /devcontainer/

# Setup devcontainer
RUN GIT_EMAIL=${GIT_EMAIL} \
    GIT_SIGNINGKEY=${GIT_SIGNINGKEY} \
        /devcontainer/basics.sh

# Setup tools
RUN /devcontainer/tools/docker.sh \
    && /devcontainer/tools/kubectl.sh

# Setup user
RUN KUBECONFIG=${KUBECONFIG} \
        /devcontainer/user/kubeconfig.sh \
    && /devcontainer/user/starship.sh

# Install VSCode GO tools
RUN /devcontainer/kits/go.sh

# Private GO modules
##ENV GOPROXY=https://proxy.golang.org,direct
#ENV GOPRIVATE=gitlab.com/geekstuff.it
```

See also the [.devcontainer](./.devcontainer/) folder in this repository for
a more complete example.

## Coming up next

- More full examples.
- Automated tests to build scripts against our larget targets.
- Ability for tools scripts to specify versions to install instead of latest.
- Basic automated tests for each scripts on both debian & alpine.

## Docker image

[geekstuffreal/devcontainer](https://hub.docker.com/r/geekstuffreal/devcontainer)

NOTE: Use tags for immutability.
